package extensions

import (
	"bitbucket.org/krkn-dev/krkn"
)

// URLLengthFilter filters out requests with URLs longer than URLLengthLimit
func URLLengthFilter(c *krkn.Collector, URLLengthLimit int) {
	c.OnRequest(func(r *krkn.Request) {
		if len(r.URL.String()) > URLLengthLimit {
			r.Abort()
		}
	})
}
